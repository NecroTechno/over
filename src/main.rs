use rodio::{Decoder, OutputStream, Sink};
use std::env::args;
use std::io::Cursor;

// initialise audio stream and play
fn play(resource: &'static [u8]) {
    let (_stream, stream_handle) =
        OutputStream::try_default().expect("Unable to create output stream!");
    let sink = Sink::try_new(&stream_handle).expect("Unable to create sink!");
    let decode_error = "Could not decode sound.";
    let source = Decoder::new(Cursor::new(resource)).expect(decode_error);
    sink.append(source);
    sink.sleep_until_end();
}

fn main() {
    let args = args().collect::<Vec<String>>();

    // if no arguments, just ples, otherwise try and determine exit code
    let prev_exit_code = if args.len() > 1 {
        fn handle_parse_err(err: std::num::ParseIntError) -> i16 {
            eprintln!("Unable parse error code: {}.", err);
            1
        }
        args[1].parse::<i16>().unwrap_or_else(handle_parse_err)
    } else {
        0
    };

    match prev_exit_code {
        0 => {
            const WOW: &[u8; 271324] = include_bytes!("../resources/wow.flac");
            play(WOW);
        }
        _ => {
            const WILHELM: &[u8; 198236] = include_bytes!("../resources/wilhelm.wav");
            play(WILHELM);
        }
    }
}
