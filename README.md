# over

Utility to alert the user when a command has finished running. Can be run by itself:

```
sh important_script.sh; over
```

It can also accept the exit code of the previous command to alert the user of the command status:

```
sh important_script.sh; over $?
```

## dependencies

- libasound2-dev